﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using HtmlAgilityPack;
using SeleniumExtras.WaitHelpers;

namespace BusinessInfoCrawling
{
    public class Processor
    {
        #region Properties 
        public ChromeDriver driver { get; set; }
        static Processor __Instance;
        public static string DebugOutFile = "LogError.txt";
        CookieContainer cookieContainer;
        WebDriverWait wait;
        IJavaScriptExecutor javaScriptExecutor;
        #endregion
        #region Singleton
        public static Processor GetInstance()
        {
            try
            {
                if (__Instance == null)
                    __Instance = new Processor();
            }
            catch { }
            return __Instance;
        }
        #endregion
        #region Dispose
        public void Dispose()
        {
            if (driver != null)
            {
                try
                {
                    driver.Dispose();
                }
                catch { }
            }
        }
        #endregion
        #region Debug
        static void DebugOut(Exception e, string header)
        {
            Debug.WriteLine(header);
            Debug.WriteLine(e);
            File.AppendAllLines(DebugOutFile, new List<string> { DateTime.Now.ToString() + ":" + header, e.ToString() });
        }
        #endregion


        #region Contructor
        Processor()
        {

        }
        #endregion
        #region Create driver
        public void CreateDriver(string user, string pass)
        {
            try
            {
                Dispose();
                ChromeOptions opt = new ChromeOptions();
                opt.AddUserProfilePreference("profile.managed_default_content_settings.images", 2);
                opt.BinaryLocation = @"Chrome\chrome.exe";
                opt.AddExcludedArgument("enable-automation");
                ChromeDriverService chromeDriverService = ChromeDriverService.CreateDefaultService();
                chromeDriverService.HideCommandPromptWindow = true;
                driver = new ChromeDriver(chromeDriverService, opt);
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromMilliseconds(1200000);
                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl("https://www.wcaworld.com/Account/Login?ReturnUrl=/MemberSection");
                wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(20000));
                wait.Until(ExpectedConditions.ElementExists(By.Id("username")));
                wait.Until(ExpectedConditions.ElementExists(By.Id("password")));
                driver.FindElementById("username").SendKeys(user);
                driver.FindElementById("password").SendKeys(pass);
                driver.FindElementByCssSelector("button[type='submit']").Click();
                while (driver.Url.ToLower().Contains("login"))
                {
                }
                driver.Navigate().GoToUrl("https://www.wcaworld.com/Directory");
            }
            catch (Exception e)
            {
                DebugOut(e, "Create driver");
            }

        }
        #endregion

        #region Crawl
        async Task<HashSet<string>> Crawl(string url)
        {
            url = Regex.Replace(url, @"pageIndex=\d+&", "");
            int pageIndex = 1;

            HashSet<String> list = new HashSet<string>();
            try
            {
                using (WebClient client = new WebClient())
                {
                    while (true)
                    {

                        string result = client.DownloadString(url + "&pageIndex=" + pageIndex++);
                        if (result.Contains("not found"))
                            break;
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(result);
                        var nodes = doc.DocumentNode.SelectNodes(@"//*[@class='directoyname']/a");
                        for (int k = 0; k < nodes.Count; k++)
                        {
                            list.Add(nodes[k].GetAttributeValue("href", ""));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                DebugOut(e, "Crawl");
            }
            return list;
        }
        async Task<List<Result>> GetInfos(List<string> urls)
        {
            List<Result> results = new List<Result>();
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            using (HttpClient client = new HttpClient(handler))
            {
                client.DefaultRequestHeaders.Add("User-Agent", @"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36 Edg/84.0.522.59");
                client.DefaultRequestHeaders.Add("Accept-Encoding", @"gzip, deflate, br");
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");
                client.DefaultRequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                client.DefaultRequestHeaders.Add("Sec-Fetch-User", "?1");
                client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                client.DefaultRequestHeaders.Add("Referer", "https://www.wcaworld.com/Directory");
                for (int i = 0; i < urls.Count; i++)
                {
                    try
                    {
                        HttpResponseMessage response = await client.GetAsync(urls[i]);
                        string responseBody = await response.Content.ReadAsStringAsync();
                        var doc = new HtmlDocument();
                        doc.LoadHtml(responseBody);
                        var elemCompany = doc.DocumentNode.SelectSingleNode("//*[@class='company']");
                        var elemID = doc.DocumentNode.SelectSingleNode("//*[contains(@class, 'compid')]");

                        var elemAddress = doc.DocumentNode.SelectSingleNode("//*[text()='Address:']/following-sibling::*");
                        var elemEmails = doc.DocumentNode.SelectNodes("//*[contains(text(),\"Email\")]/following-sibling::*");
                        var elemNames = doc.DocumentNode.SelectNodes("//*[contains(text(),\"Name\")]/following-sibling::*");
                        var elemTitles = doc.DocumentNode.SelectNodes("//*[contains(text(),\"Title\")]/following-sibling::*");

                        string company = elemCompany?.InnerText ?? "Not Available";
                        string address = elemAddress?.InnerText ?? "Not Available";
                        string id = elemID?.InnerText ?? "Not Available";

                        string contactEmail = "";


                        int maxInstance = -1;

                        int nameCount = elemNames?.Count ?? 0;
                        int emailCount = elemEmails?.Count - 1 ?? 0;
                        int titleCount = elemTitles?.Count ?? 0;
                        if (nameCount < emailCount)
                        {
                            if (emailCount < titleCount)
                            {
                                maxInstance = titleCount;
                            }
                            else
                                maxInstance = emailCount;
                        }
                        else
                        {
                            if (nameCount < titleCount)
                            {
                                maxInstance = titleCount;
                            }
                            else
                                maxInstance = nameCount;
                        }
                        if (maxInstance == 0)
                        {
                            results.Add(new Result()
                            {
                                ID = id,
                                Company = company,
                                Address = address,
                                ContactEmail = contactEmail,
                                Email = "",
                                Name = "",
                                Title = ""
                            });
                        }
                        for (int k = 0; k < maxInstance; k++)
                        {
                            results.Add(new Result()
                            {
                                ID = id,
                                Company = company,
                                Address = address,
                                ContactEmail = contactEmail,
                                Email = emailCount > k ? elemEmails[k + 1].InnerText.Trim() : "",
                                Name = nameCount > k ? elemNames[k].InnerText.Trim() : "",
                                Title = titleCount > k ? elemTitles[k].InnerText.Trim() : ""
                            });
                        }

                        Debug.WriteLine(urls[i] + " " + address + "," + contactEmail);
                    }
                    catch (Exception e)
                    {
                        DebugOut(e, "Get Infos" + urls[i]);
                    }
                }
                return results;
            }
        }
        #endregion
        #region Entry
        public async Task<List<Result>> Begin()
        {
            List<Result> results = new List<Result>();
            try
            {
                cookieContainer = new CookieContainer();
                CookieCollection cc = new CookieCollection();
                foreach (OpenQA.Selenium.Cookie cook in driver.Manage().Cookies.AllCookies)
                {
                    System.Net.Cookie cookie = new System.Net.Cookie();
                    cookie.Name = cook.Name;
                    cookie.Value = cook.Value;
                    cookie.Domain = cook.Domain;
                    cc.Add(cookie);
                }
                cookieContainer.Add(cc);
                results = await GetInfos((await Crawl(driver.Url)).Where(url => !url.Contains("http")).Select(url => { if (url.Contains("http")) return url.Trim(); else return "https://www.wcaworld.com" + url.Replace("href=\"", "").Replace("href=\'", "").Trim(); }).ToList());
            }
            catch (Exception e)
            {
                DebugOut(e, "Begin");
            }
            return results;
        }
        #endregion
    }
}
