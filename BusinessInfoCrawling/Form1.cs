﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BusinessInfoCrawling
{
    public partial class Form1 : Form
    {

        Processor processor;
        public Form1()
        {
            InitializeComponent();
            processor = Processor.GetInstance();
        }

        private void openChromeBtn_Click(object sender, EventArgs e)
        {
            processor.CreateDriver("fclhcvin", "848227");
        }

        private async void exportBtn_Click(object sender, EventArgs e)
        {
            exportBtn.Enabled = false;
            exportBtn.Text = "Exporting";
            var results =  await processor.Begin(); 
            using (var writer = new StreamWriter("Output\\" + $"{DateTime.Now.ToString("dd-MM-yyy_hh-mm-ss")}.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(results);
            }
            exportBtn.Enabled = true;
            exportBtn.Text = "Export to csv";
            MessageBox.Show("Done!");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            processor.Dispose();
        }
    }
}
