﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessInfoCrawling
{
    public class Result
    {
        public string Company { get; set; }
        public string ID { get; set; }
        public string Address { get; set; }
        public string ContactEmail { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }

    }
}
