﻿namespace BusinessInfoCrawling
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openChromeBtn = new System.Windows.Forms.Button();
            this.exportBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openChromeBtn
            // 
            this.openChromeBtn.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.openChromeBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.openChromeBtn.Location = new System.Drawing.Point(12, 34);
            this.openChromeBtn.Name = "openChromeBtn";
            this.openChromeBtn.Size = new System.Drawing.Size(206, 123);
            this.openChromeBtn.TabIndex = 0;
            this.openChromeBtn.Text = "OPEN CHROME";
            this.openChromeBtn.UseVisualStyleBackColor = false;
            this.openChromeBtn.Click += new System.EventHandler(this.openChromeBtn_Click);
            // 
            // exportBtn
            // 
            this.exportBtn.BackColor = System.Drawing.Color.DarkCyan;
            this.exportBtn.Location = new System.Drawing.Point(246, 34);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(209, 123);
            this.exportBtn.TabIndex = 1;
            this.exportBtn.Text = "EXPORT TO CSV";
            this.exportBtn.UseVisualStyleBackColor = false;
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 187);
            this.Controls.Add(this.exportBtn);
            this.Controls.Add(this.openChromeBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "WCA World (Demo)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button openChromeBtn;
        private System.Windows.Forms.Button exportBtn;
    }
}

